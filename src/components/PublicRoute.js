import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isLogin } from '../utils/auth';

const PublicRoute = ({ component: Component, restricted, ...rest }) => (

  // Need fix Redirect
  <Route
    {...rest}
    render={(props) => (isLogin() ? (
      <Component {...props} />
    ) : (
      <Redirect
        to={{
          pathname: '/dashboard',
        }}
      />
    ))}
  />
);

export default PublicRoute;
