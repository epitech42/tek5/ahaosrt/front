FROM node:10-alpine
WORKDIR /usr/src/app

COPY package.* .

RUN npm install

CMD ["npm", "run", "start"]
